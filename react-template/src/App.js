import './App.css';
import SagaComponent from './Component/Redux-saga-component/Redux-saga-component'
import ThunkComponet from './Component/Redux-thunk-component/Redux-thunk-component'

function App() {
  return (
    <div className="App">
      <ThunkComponet />

      <SagaComponent />
    </div >
  );
}

export default App;
