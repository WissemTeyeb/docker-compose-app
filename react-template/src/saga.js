import { all } from 'redux-saga/effects';
import { actionWatcher } from './Component/Redux-saga-component/api'
export default function* rootSaga() {
    yield all([
        actionWatcher(),
    ]);
}
